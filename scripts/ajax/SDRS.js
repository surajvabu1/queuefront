if(typeof SDR !== 'object'){
    var SDR = {};
}

(function (){

    'use strict';

    var requestHeaders = {"Accept":"application/json", "Content-Type":"application/json"};

    function request(url, request_type, datatype,callback,postdata){

        // For debug only
        var SERVER = "http://localhost:8080"; /*"http://52.27.46.230:8080";*/
        url = SERVER+url;
        console.log(url);
        if(typeof(postdata) == "object"){
            postdata = JSON.stringify(postdata);

        }

        $.ajax({
            beforeSend: function(xhr){
                for(var key in requestHeaders){
                    xhr.setRequestHeader(key, requestHeaders[key]);
                    // ajaxindicatorstart("...Please wait.. Data is loading...");
                }
            },
            url: url,
            xhrFields: {
                withCredentials: true
            },
            data: postdata,
            dataType: datatype,
            type: request_type,
            success: function(data) {
                // ajaxindicatorstop();
                callback("success", data);
            },
            error: function(xhr, status, error) {
                // ajaxindicatorstop();
                try{
                    var errorMessageObj = JSON.parse(xhr.responseText);
                }catch(err){
                    errorMessageObj = {type:"ERROR", message:xhr.responseText};
                }
                callback(error, errorMessageObj);
            }
        });
    }

    SDR.login = function(username,password,callback) {
        var url = "/QueueBack/login/login";
        alert (username +" "+ password);
        var memberDetails = {
            username: username,
            password: password
        };
        request(url,"POST","json",callback, memberDetails);
    };

    SDR.logout = function(callback) {
        var url = "/QueueBack/login/logout";
        request(url,"POST","json",callback);
    };

    SDR.generatetoken = function(callback) {
        var url = "/QueueBack/queue"
        request(url,"POST","json",callback);
    }
}());