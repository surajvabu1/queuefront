
function login(){
    var username = $("#username").val();
    var password = $("#password").val();

    SDR.login(username,password,loginfunc());

}

function loginfunc(){
    return function(status, data){
        alert(JSON.stringify(data));
        if(data.connection == "success") {
            window.location="index.html";
        }
    }
}

function logout(){
    SDR.logout(logoutfunc());
}

function logoutfunc(){
    return function(status, data){
        if(data.connection == "success") {
            window.location="login.html";
        }
    }
}

function generatetoken() {
    SDR.generatetoken(generatetokencallback())
}

function generatetokencallback() {
    return function(status, data){
        if(data.connection == "success") {
            alert(JSON.stringify(data));
        }
    }
}